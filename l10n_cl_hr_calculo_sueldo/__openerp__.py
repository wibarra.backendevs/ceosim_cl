# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2009  Àngel Àlvarez - NaN  (http://www.nan-tic.com)
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Calculo de Sueldo Líquido',
    'version': '8.0',
    'category': 'Recursos Humanos',
    'sequence': 14,
    'summary': 'Calculo de Sueldo Liquido',
    'description': """
Calculo de Sueldo Líquido
============
Simulador para el Calculo de Sueldo Líquido en Chile:
----------
¿Quieres saber cual será el dinero que recibiras mensualmente en tu bolsillo?¿Estas negociando un sueldo y no sabes el sueldo bruto que debes pedir?

Ingresa tu sueldo base, gratificaciones, comisiones, bonos, AFP e isapre y la demás información para calcula el dinero final que recibiras en tu bolsillo a final de mes. [calculadora más abajo].

Esta herramienta es para calcular el sueldo líquido para contratos mensuales, si quieres puedes utilizar la herramienta de cáclulo de sueldo líquido quincenal , semanal , diario.
  """,
    'author':  'Wilmer Ibarra',
    'license': 'AGPL-3',
    'images': [
    ],
    'depends': [
        'hr','l10n_cl_hr_payroll', 'hr_contract', 'hr_finiquito_cl',
    
    ],
    'data': [
        'views/simulador_sueldo_liquido.xml'      
    ],
    'demo': [
        #'demo/demo_data.xml',
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
