#-*- coding: utf-8 -*-
from openerp import models, fields, api
from datetime import datetime, date, time, timedelta
from dateutil import relativedelta as rdelta
from datetime import date , datetime
from openerp import exceptions



class SimuladorSueldoLiquido(models.Model):
    _name = 'simulador.sueldo.liquido'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    #Indicadores
    indicadores_id = fields.Many2one('hr.indicadores', string='Indicadores',)
    utm = fields.Float(related='indicadores_id.utm', string='UTM', store=True,)
    uf = fields.Float(related='indicadores_id.uf', string='UF', store=True,)
    imm = fields.Float(related="indicadores_id.sueldo_minimo", string='IMM', store=True,)
    sueldo_minimo = fields.Float(related="indicadores_id.sueldo_minimo", string='Sueldo Mínimo',digits=(32,0),)

    #Contrato
    #contrato = fields.Selection([('mensual','Mensual'),('quincenal','Quincenal'),('semanal','Semanal'),('diario','Diario')],default='mensual')
    contrato = fields.Selection([('mensual','Mensual')],default='mensual')
    
    plazo_contrato = fields.Selection([('indefinido','Indefinido'),('fijo','Fijo')],default='indefinido')

    #Haberes
    sueldo_base = fields.Float(string='Sueldo Base',digits=(32,0),store=True,help='Si tu sueldo base ya tiene incluia la gratificación, favor quitar el item de gratificación', )
    dias_trabajados = fields.Integer(string='Días trabajados', default=30)
    gratificacion = fields.Selection([('si','Si'),('no','No'),('editar','Editar')],default='no')
    valor_gratificacion = fields.Float(string='Valor gratifiación', digits=(32,0))
    valor_gratificacion_editar = fields.Float(string='Valor gratifiación',help="Al incluir, automáticamente se calcula el 25% del sueldo base o 4,75 sueldos mínimos mensuales o mensualizado. Lo que sea menor.")
    horas_extras = fields.Integer(string='Horas Extras', placedhol='Cantidad')
    valor_horas_extras = fields.Integer(string='Valor Horas Extras', help='Horas extraordinarias calculadas para un trabajador con jornada de 45 horas semanales y con un recargo de 50%', )
    comisiones = fields.Integer(string='Comisiones (opcional)',)
    bonos = fields.Integer(string='Bonos (opcional)',)
    colacion = fields.Integer(string='Colación (opcional)',)
    movilizacion = fields.Integer(string='Movilización (opcional)',)
    total_haberes = fields.Float(string='Haberes',)

    #Descuentos
    afp = fields.Selection([('capital','Capital'),('cuprum','Cuprum'),('habitat','Habitat'),('planvital','Plan Vital'),('provida','Pro Vida'),('modelo','Modelo')],string="AFP")
    valor_afp_capital = fields.Float(related='indicadores_id.tasa_independiente_capital',string='Total AFP', help='Corresponde al 10% de previsión más el porcentaje correspondiente a comisión de AFP', )
    valor_afp_cuprum = fields.Float(related='indicadores_id.tasa_independiente_cuprum',string='Total AFP',help='Corresponde al 10% de previsión más el porcentaje correspondiente a comisión de AFP',)
    valor_afp_habitat = fields.Float(related='indicadores_id.tasa_independiente_habitat',string='Total AFP',help='Corresponde al 10% de previsión más el porcentaje correspondiente a comisión de AFP',)
    valor_afp_planvital = fields.Float(related='indicadores_id.tasa_independiente_planvital',string='Total AFP',help='Corresponde al 10% de previsión más el porcentaje correspondiente a comisión de AFP',)
    valor_afp_provida = fields.Float(related='indicadores_id.tasa_independiente_provida',string='Total AFP',help='Corresponde al 10% de previsión más el porcentaje correspondiente a comisión de AFP',)
    valor_afp_modelo = fields.Float(related='indicadores_id.tasa_independiente_modelo',string='Total AFP',help='Corresponde al 10% de previsión más el porcentaje correspondiente a comisión de AFP',)   
    prevision = fields.Selection([('fonasa','Fonasa 7%'),('isapreuf','Isapre UF')],string='Previsión')
    total_salud_fonasa = fields.Float(related='indicadores_id.fonasa', string='Total salúd',help='Corresponde al 7% de la base imponible o en el caso de Isapres es un monto pactado en UF',)
    total_salud_isapreuf = fields.Float(related='indicadores_id.uf', string='Total salud',help='Corresponde al 7% de la base imponible o en el caso de Isapres es un monto pactado en UF', )
    seguro_cesantia = fields.Float(string="Seguro cesantía",help='Corresponde al 0.6% de la base imponible para el seguro de cesantía', )
    seguro_cesantia_indefinido = fields.Float(related ='indicadores_id.contrato_plazo_fijo_trabajador', string='Seguro cesantía Indefinido',)
    impuesto = fields.Float(string='Impuesto',help='El impuesto único de segunda categría se calcula en escalas dependiendo de la base tributable, siempre y cuando la bse tributable sea mayor a 13.5 UTM mensual.', )
    apv = fields.Float(string='APV (opcional)')
    otros_descuentos = fields.Float(string='Otros descuentos (opcional)',)

    #Resultados
    total_haberes = fields.Float(string='Haberes',help='Es el dinero que desembolsa el empleador por el empleado.También conocido como sueldo bruto.', )
    total_descuentos = fields.Float(string='Descuentos',help='Corresponde a todos los descuentos realizados al Total haberes', )
    sueldo_liquido = fields.Float(string='Sueldo liquido',help='Corresponde al sueldo que finalmente recibirás en tu bolsillo. Líquido=Total haberes-Descuentos', )


    #Calculo de Impuesto Luia
#     if payslip.indicadores_id.utm*13.5 > TRIBU :
#     result = 0
# elif payslip.indicadores_id.utm*30 > TRIBU:
#  result = TRIBU*0.04-(payslip.indicadores_id.utm*0.54)
# elif payslip.indicadores_id.utm*50 > TRIBU:
#  result = TRIBU*0.08-(payslip.indicadores_id.utm*1.74)
# elif payslip.indicadores_id.utm*70 > TRIBU:
#  result = TRIBU*0.135-(payslip.indicadores_id.utm*4.49)
# elif payslip.indicadores_id.utm*90 > TRIBU:
#  result = TRIBU*0.23-(payslip.indicadores_id.utm*11.14)
# elif payslip.indicadores_id.utm*120 > TRIBU:
#  result = TRIBU*0.304-(payslip.indicadores_id.utm*17.85)
# elif payslip.indicadores_id.utm*150 > TRIBU:
#  result = TRIBU*0.355-(payslip.indicadores_id.utm*23.92)
# else:
#  result = TRIBU*0.40-(payslip.indicadores_id.utm*30.67)


# TRIBU = sumatoria de: Sueldo Base + Horas Extras + Gratificación + Bonos + Comisiones.

    #Calculo Total Haberes
    @api.onchange('sueldo_base','gratificacion','valor_horas_extras','valor_gratificacion_editar','valor_gratificacion','bonos','comisiones','movilizacion')
    def _onchange_total_haberes(self):
        if self.gratificacion == 'si':
            self.total_haberes = self.sueldo_base + self.valor_gratificacion + self.valor_horas_extras + self.comisiones + self.bonos + self.colacion + self.movilizacion
        elif self.gratificacion == 'editar':
            self.total_haberes = self.sueldo_base + self.valor_gratificacion_editar + self.valor_horas_extras + self.comisiones + self.bonos + self.colacion + self.movilizacion
       
    #Calculo Descuents
    @api.onchange('afp','prevision','valor_afp_capital','valor_afp_cuprum','valor_afp_habitat','valor_afp_planvital','valor_afp_provida','valor_afp_modelo','total_salud_fonasa','total_salud_isapreuf','seguro_cesantia','impuesto','apv','otros_descuentos',)
    def _onchange_total_descuentos(self):
        if self.afp == 'capital' and self.prevision == 'fonasa':
            self.total_descuentos = self.valor_afp_capital + self.total_salud_fonasa + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'capital' and self.prevision == 'isapreuf':
            self.total_descuentos = self.valor_afp_capital + self.total_salud_isapreuf + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'cuprum' and self.prevision == 'fonasa':
            self.total_descuentos = self.valor_afp_cuprum + self.total_salud_fonasa + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'cuprum' and self.prevision == 'isapreuf':
            self.total_descuentos = self.valor_afp_cuprum + self.total_salud_isapreuf + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'habitat' and self.prevision == 'fonasa':
            self.total_descuentos = self.valor_afp_habitat + self.total_salud_fonasa + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'habitat' and self.prevision == 'isapreuf':
            self.total_descuentos = self.valor_afp_habitat + self.total_salud_isapreuf + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'planvital' and self.prevision == 'fonasa':
            self.total_descuentos = self.valor_afp_planvital + self.total_salud_fonasa + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'planvital' and self.prevision == 'isapreuf':
            self.total_descuentos = self.valor_afp_planvital + self.total_salud_isapreuf + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'provida' and self.prevision == 'fonasa':
            self.total_descuentos = self.valor_afp_provida + self.total_salud_fonasa + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'provida' and self.prevision == 'isapreuf':
            self.total_descuentos = self.valor_afp_provida + self.total_salud_isapreuf + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'modelo' and self.prevision == 'fonasa':
            self.total_descuentos = self.valor_afp_modelo + self.total_salud_fonasa + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'modelo' and self.prevision == 'isapreuf':
            self.total_descuentos = self.valor_afp_modelo + self.total_salud_isapreuf + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos

    #Calculo Sueldo Liquido
    @api.onchange('total_haberes','total_descuentos')
    def _onchange_sueldo_liquido(self):
        self.sueldo_liquido = self.total_haberes - self.total_descuentos
        


    #Calculo de Impuesto Wilmer
    @api.onchange('indicadores_id','sueldo_base','horas_extras','gratificacion','valor_gratificacion','valor_gratificacion_editar','bonos','comisiones')
    def _onchange_impuesto(self):
        if self.gratificacion == 'si':
            tribu_s = self.sueldo_base + self.valor_horas_extras + self.valor_gratificacion + self.bonos + self.comisiones
            if self.utm*13.5 > tribu_s:
                self.impuesto = 0
            elif self.utm*30 > tribu_s:
                self.impuesto = tribu_s*0.04-(self.utm*0.54)
            elif self.utm*50 > tribu_s:
                self.impuesto = tribu_s*0.08-(self.utm*1.74)
            elif self.utm*70 > tribu_s:
                self.impuesto = tribu_s*0.135-(self.utm*4.49)
            elif self.utm*90 > tribu_s:
                self.impuesto = tribu_s*0.23-(self.utm*11.14)
            elif self.utm*120 > tribu_s:
                self.impuesto = tribu_s*0.304-(self.utm*17.85)
            elif self.utm*150 > tribu_s:
                self.impuesto = tribu_s*0.355-(self.utm*23.92)
            else:
                self.impuesto = tribu_s*0.40-(self.utm*30.67)
        elif self.gratificacion == 'editar':
            tribu_e = self.sueldo_base + self.valor_horas_extras + self.valor_gratificacion_editar + self.bonos + self.comisiones
            if self.utm*13.5 > tribu_e:
                self.impuesto = 0
            elif self.utm*30 > tribu_e:
                self.impuesto = tribu_e*0.04-(self.utm*0.54)
            elif self.utm*50 > tribu_e:
                self.impuesto = tribu_e*0.08-(self.utm*1.74)
            elif self.utm*70 > tribu_e:
                self.impuesto = tribu_e*0.135-(self.utm*4.49)
            elif self.utm*90 > tribu_e:
                self.impuesto = tribu_e*0.23-(self.utm*11.14)
            elif self.utm*120 > tribu_e:
                self.impuesto = tribu_e*0.304-(self.utm*17.85)
            elif self.utm*150 > tribu_e:
                self.impuesto = tribu_e*0.355-(self.utm*23.92)
            else:
                self.impuesto = tribu_e*0.40-(self.utm*30.67)

            
    #calculo seguro de cesantia
    @api.onchange('plazo_contrato','gratificacion')
    def _onchange_plazo_contrato(self):
        if self.plazo_contrato == 'indefinido' and self.gratificacion == 'si':
            indicador = self.seguro_cesantia_indefinido
            self.seguro_cesantia = indicador/100*(self.sueldo_base+self.valor_horas_extras+self.valor_gratificacion+self.bonos+self.comisiones)
        elif self.plazo_contrato == 'indefinido' and self.gratificacion == 'editar':
            indicador = self.seguro_cesantia_indefinido
            self.seguro_cesantia = indicador/100*(self.sueldo_base+self.valor_horas_extras+self.valor_gratificacion_editar+self.bonos+self.comisiones)
        else:
            self.seguro_cesantia = 0.0
        


    #calulo de valor horas extras
    @api.onchange('sueldo_base','horas_extras','dias_trabajados')
    def _onchange_sueldo_base(self):
        self.valor_horas_extras = self.sueldo_base / 30.0 * 0.0077777 * self.dias_trabajados * self.horas_extras
    

   
    #consulta Sueldo Minimo
    @api.one
    def _compute_sueldo_minimo(self):
        indicadores = self.env['indicadores.finiquito'].search([('name','=','Sueldo mínimo')])
        if len(indicadores)>0:            
            valor_sueldo_minimo = indicadores.valor_indicador
            self.sueldo_minimo = valor_sueldo_minimo
        else:
            raise exceptions.ValidationError('No existe un valor del Sueldo mínimo')

    #calculo de la gratificacion
    @api.onchange('gratificacion','sueldo_base','valor_horas_extras','bonos','comisiones')
    def _onchange_gratificacion(self):
        #calcular gratificacion
        if self.gratificacion == 'si':
            # if self.sueldo_base * 0.25 < self.sueldo_minimo * 4.75:
            #     self.valor_gratificacion = self.sueldo_base * 0.25
            # else:
            #     self.valor_gratificacion = self.sueldo_minimo * 4.75
            self.valor_gratificacion = round(((self.sueldo_base + self.valor_horas_extras + self.bonos + self.comisiones)*0.25))
        elif self.gratificacion == 'no':
            self.valor_gratificacion = 0
        elif self.gratificacion == 'editar':
            self.valor_gratificacion = self.valor_gratificacion_editar


    @api.one
    def _compute_total_haberes(self):
        self.total_haberes = self.sueldo_base + self.valor_gratificacion + self.valor_horas_extras + self.comisiones + self.bonos + self.colacion + self.movilizacion

    @api.one
    def _compute_total_descuentos(self):
        if self.afp == 'capital' and self.prevision == 'fonasa':
            self.total_descuentos = self.valor_afp_capital + self.total_salud_fonasa + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'capital' and self.prevision == 'isapreuf':
            self.total_descuentos = self.valor_afp_capital + self.total_salud_isapreuf + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'cuprum' and self.prevision == 'fonasa':
            self.total_descuentos = self.valor_afp_cuprum + self.total_salud_fonasa + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'cuprum' and self.prevision == 'isapreuf':
            self.total_descuentos = self.valor_afp_cuprum + self.total_salud_isapreuf + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'habitat' and self.prevision == 'fonasa':
            self.total_descuentos = self.valor_afp_habitat + self.total_salud_fonasa + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'habitat' and self.prevision == 'isapreuf':
            self.total_descuentos = self.valor_afp_habitat + self.total_salud_isapreuf + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'planvital' and self.prevision == 'fonasa':
            self.total_descuentos = self.valor_afp_planvital + self.total_salud_fonasa + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'planvital' and self.prevision == 'isapreuf':
            self.total_descuentos = self.valor_afp_planvital + self.total_salud_isapreuf + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'provida' and self.prevision == 'fonasa':
            self.total_descuentos = self.valor_afp_provida + self.total_salud_fonasa + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'provida' and self.prevision == 'isapreuf':
            self.total_descuentos = self.valor_afp_provida + self.total_salud_isapreuf + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'modelo' and self.prevision == 'fonasa':
            self.total_descuentos = self.valor_afp_modelo + self.total_salud_fonasa + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos
        elif self.afp == 'modelo' and self.prevision == 'isapreuf':
            self.total_descuentos = self.valor_afp_modelo + self.total_salud_isapreuf + self.seguro_cesantia + self.impuesto + self.apv + self.otros_descuentos

    @api.one
    def _compute_sueldo_liquido(self):
        self.sueldo_liquido = self.total_haberes - self.total_descuentos



    @api.one
    def _compute_valor_afp(self):
        indicadores = self.indicadores_id
        if self.aft == "capital":
            capital = indicadores_id.tasa_independiente_capital
            self.valor_afp = capital
        elif self.aft == "cuprum":
            self.valor_afp = self.indicadores_id.tasa_independiente_cuprum
        elif self.aft == 'habitat':
            self.valor_afp = self.indicadores_id.tasa_independiente_habitat
        elif self.aft == 'planvital':
            self.valor_afp = self.indicadores_id.tasa_independiente_planvital
        elif self.aft == 'provida':
            self.valor_afp = self.indicadores_id.tasa_independiente_provida
        elif self.aft == 'modelo':
            self.valor_afp = self.indicadores_id.tasa_independiente_modelo






    #Variables
    state = fields.Selection([('borrador','Borrador'),('confirmado','Confirmado')],'Estado',default='borrador',help='Cuando se Confirma el Finiquito se colocará el estado del contrato en Finiquitado')
    #sueldo_minimo = fields.Float(string='Sueldo Líquido',compute='_compute_sueldo_minimo',digits=(32,0))
    bono_colacion = fields.Float(string='Bono Colación',help='Importe establecido en su contrato',digits=(32,0),store=True,)
    bono_movilizacion = fields.Float(string='Bono Movilización',help='Importe establecido en su contrato',digits=(32,0),store=True,)
    impuesto_unico_negativo = fields.Float(string='Impuesto único en Negativo',)   
    otros = fields.Float(string='Otros',digits=(32,0),store=True,)
    imponile_neto = fields.Float(string='Imponible Neto', compute='_compute_imponible_neto')
    afp_capital= fields.Float(string='AFP Capital',)
    afp_cuprum = fields.Float(string='AFP Cuprum',)
    afp_habitat = fields.Float(string='AFP Habitat',)
    afp_plan_vital = fields.Float(string='AFP Plan Vital',)
    afp_provida = fields.Float(string='AFP Provida',) 
    afp_modelo = fields.Float(string='AFP Modelo',)
    salud = fields.Float(string='Salud',)
    plazo_fijo = fields.Float(string='Plazo Fijo',)
    plazo_indefinido_sc = fields.Float(string='Plazo Indefinido SC',)
    total = fields.Float(string='TOTAL',)
    imponible_bruto = fields.Float(string='Imponible Bruto',)
    sin_gratificacion = fields.Float(string='sin gratifiación',)
    con_gratificacion = fields.Float(string='Gratificación',)
    total_sueldo_base = fields.Float(string='SUELDO BASE',compute='_compute_total_sueldo_base')

    #variables para el CALCULO HORAS DE ATRASO
    sueldo_base_horas_atraso = fields.Float(string='Sueldo Base',compute='_compute_sueldo_base')
    divide_treinta_dias = fields.Integer(string='Divide entre 30 días', default=30)
    subtotal_horas_atraso = fields.Float(string='SUB TOTAL', compute='_compute_sutotal_horas_atraso')
    multiplica_por_veniocho = fields.Integer(string='Multiplica por 28 (7X4 Semanas)', default=28)
    sub_total_por_ventiocho = fields.Float(string='SUB TOTAL', compute='_compute_multiplica_por_ventiocho')
    divide_por_cientoochenta  = fields.Integer(string='Divide por 180 (45 HORAS Semanales por 4 Semanas)',default=180)
    valor_hora_atraso = fields.Float(string='Valor Hora de Atraso', compute='_compute_valor_hora_atraso')
    por_las_horas_de_atraso = fields.Integer(string='Por las horas de atraso',)
    descuento_horas_atraso = fields.Float(string='Descuento Horas atraso', compute='_compute_descuento_horas_atraso')

    #variables para calculo horas extras
    sueldo_base_horas_he = fields.Float(string='Sueldo Base',compute='_compute_sueldo_base')
    divide_treinta_dias_he = fields.Integer(string='Divide entre 30 días', default=30)
    subtotal_horas_extras = fields.Float(string='SUB TOTAL', compute='_compute_sutotal_horas_atraso')
    multiplica_por_veniocho_he = fields.Integer(string='Multiplica por 28 (7X4 Semanas)', default=28)
    sub_total_por_ventiocho_he = fields.Float(string='SUB TOTAL', compute='_compute_multiplica_por_ventiocho')
    divide_por_cientoochenta_he  = fields.Integer(string='Divide por 180 (45 HORAS Semanales por 4 Semanas)',default=180)
    valor_hora_atraso_he = fields.Float(string='Valor Hora de Atraso', compute='_compute_valor_hora_atraso_he')
    porcentaje_hora_extra = fields.Integer(string='Porcentaje hora extra',)
    valor_hora_extra = fields.Float(string='Valor Hora Extra',)
    por_las_horas_extras = fields.Integer(string='Por las horas extras',)
    total_en_horas_extras = fields.Float(string='Total en horas extras',)

    #calculo valor horas atraso
    @api.multi
    def _compute_valor_hora_atraso(self):
        self.valor_hora_atraso = self.sub_total_por_ventiocho / self.divide_por_cientoochenta
    
    #calculo valor horas atraso extra
    @api.multi
    def _compute_valor_hora_atraso_he(self):
        self.valor_hora_atraso_he = self.sub_total_por_ventiocho_he / self.divide_por_cientoochenta_he

    #calculo descuento horas atraso
    @api.multi
    def _compute_descuento_horas_atraso(self):
        self.descuento_horas_atraso = self.valor_hora_atraso * self.por_las_horas_de_atraso * (-1)

    #calculo _compute_multiplica_por_ventiocho
    @api.multi
    def _compute_multiplica_por_ventiocho(self):
        self.multiplica_por_veniocho = self.subtotal_horas_atraso * 28

    #calculo sub total horas atraso
    @api.multi
    def _compute_sutotal_horas_atraso(self):
        self.sueldo_base_horas_atraso = self.sueldo_base_horas_atraso / self.divide_treinta_dias

    #calculo sueldo base horas de atraso
    @api.multi
    def _compute_sueldo_base(self):
        self.sueldo_base_horas_atraso = self.total_sueldo_base

    #calculo total sueldo base
    @api.multi
    def _compute_total_sueldo_base(self):
        self.total_sueldo_base = self.imponible_bruto + self.sin_gratificacion + self.con_gratificacion

     #consulta Sueldo Minimo
    # @api.one
    # def _compute_sueldo_minimo(self):
    #     indicadores = self.env['indicadores.finiquito'].search([('name','=','Sueldo mínimo')])
    #     if len(indicadores)>0:            
    #         valor_sueldo_minimo = indicadores.valor_indicador
    #         self.sueldo_minimo = valor_sueldo_minimo
    #     else:
    #         raise exceptions.ValidationError('No existe un valor del Sueldo mínimo')

    #calculo del imponible neto
    @api.multi
    def _compute_imponible_neto(self):
        self.imponile_neto = self.sueldo_minimo - self.bono_colacion - self.bono_movilizacion - self.impuesto_unico_negativo - self.otros


    
