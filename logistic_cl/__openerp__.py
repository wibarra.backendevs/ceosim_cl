# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015  ADHOC SA  (http://www.adhoc.com.ar)
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{'active': False,
    'author': 'Wilmer Ibarra',
    'category': 'logistic',
    'demo': [
    ],
    'depends': ['logistic','crm','fleet'],
    'description': """
Logistic ceosim
""",
    'installable': True,
    'auto_install': False,
    'application': False,
    'license': 'AGPL-3',
    'name': 'Logistic ceosim',
    'test': [],
    'data': [
        'views/waybill.xml',
        'views/logistic_menuitem.xml',
        #'views/fleet_vehicle_type.xml',
        'views/partner_view.xml',
        'views/logistic_requeriment_view.xml',
    ],
    'version': '8.0.0.1.1',
    'css': [
    ],
    'website': 'www.nuevagestion.cl',}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
