# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in module root
# directory
##############################################################################


from openerp import models, fields, api

class waybill(models.Model):

    _inherit = 'logistic.waybill'

    # _states_ = [
        
    #     ('draft','Borrador'),
    #     ('active', 'Active'),
    #     ('confirmado', 'Confirmado'),
    #     ('rendicion','Rendicion'),
    #     ('closed', 'Closed'),
    #     ('cancelled', 'Cancelled'),
    #     ]
    
    # _track = {
    #     'state': {
    #         'logistic.waybill_active': (
    #             lambda self, cr, uid, obj, ctx=None: obj['state'] == 'active'),
    #         'logistic.waybill_closed': (
    #             lambda self, cr, uid, obj, ctx=None: obj['state'] == 'closed'),
    #         'logistic.waybill_cancelled': (
    #             lambda self, cr, uid, obj, ctx=None:
    #             obj['state'] == 'cancelled'),
    #     },
    # }
        

    vehicle_id = fields.Many2one(
        'fleet.vehicle',
        string='Vehicle',
        readonly=True,
        required=True,
        states={'active': [('readonly', False)]},
        on_change='on_change_vehicle(vehicle_id, context)'
        )

    vehicle_status = fields.Selection(
        related='vehicle_id.requirement_state',
        string='Vehicle Status'
        )
        
    #tipo_vehiculo = fields.Many2one('fleet.vehicle.type',string='Tipo de Vehículo')
    type = fields.Selection(
        [('car', 'Car'), ('tractor', 'Tractor'), ('wagon', 'Wagon')],
        string='Type')

    tractor_id = fields.Many2one(
        required=False,
        )
        
    wagon_id = fields.Many2one(
        required=False,
        )        
        
    # state = fields.Selection(
    #     _states_,
    #     "State",
    #     default='active'
    #     )
    
    litros_calculados = fields.Float(
        string='Litros calculados',
        #compute='_compute_litros_calcs'
    )

    #@api.depends('final_liters','initial_liters')
    @api.onchange('final_liters','initial_liters')
    def _onchange_final_liters(self):
        self.litros_calculados = self.initial_liters - self.final_liters
        

    # @api.one
    # def _compute_litros_calcs(self):
    # 	self.litros_calculados = self.final_liters - self.initial_liters
    #     
    initial_odometer = fields.Float(
        inverse='_set_initial_odometer',
        compute='_get_initial_odometer',
        string='Initial Odometer',
        readonly=True,
        required=False,
        states={'active': [('readonly', False)]}
        )

    inicial_odometro = fields.Float(
        related='vehicle_id.odometer',
        string='Inicial',
        required=True,
        )

    final_odometro = fields.Float(
        #inverse='_set_final_odometro',
        #compute='_get_final_odometro',
        string='Final',
        readonly=True,
        states={'active': [('readonly', False)]}
        )

    # @api.one
    # def _set_inicial_odometro(self):
    #     if not self.inicial_odometro:
    #         return True
    #     date = self.date
    #     if not self.date:
    #         date = fields.date.today()
    #     data = {'value': self.inicial_odometro, 'date': date,
    #             'vehicle_id': self.vehicle_id.id}
    #     odometer_id = self.env['fleet.vehicle.odometer'].create(data)
    #     #self.initial_odometer_id = odometer_id
    #     self.inicial_odometro = self.vehicle_id.odometer.value

    # @api.one
    # def _get_inicial_odometro(self):
    #     if vehicle_id:
    #         self.inicial_odometro = self.vehicle_id.odometer.value
        # if self.final_odometer_id:
        #     self.final_odometro = self.final_odometer_id.value