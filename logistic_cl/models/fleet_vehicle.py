# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in module root
# directory
##############################################################################


from openerp import models, fields, api

class FleetVehicle(models.Model):

    _inherit = 'fleet.vehicle'   

    _requirement_states = [
        # State machine: requirement_basic
        ('ok', 'OK'),
        ('next_to_renew', 'Next To Renew'),
        ('need_renew', 'Need Renew'),
    ]

    requirement_state = fields.Selection(
        selection=_requirement_states,
        string="Requerimientos Estatales", compute='_get_requirement_state')