# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in module root
# directory
##############################################################################


from openerp import models, fields, api

class FleetVehicleType(models.Model):

	_name = 'fleet.vehicle.type'
	_order = 'sequence asc'

	name = fields.Char(string='Nombre',)
	sequence = fields.Integer(string='Sequence',)
	_sql_constraints = [
	    ('fleet_type_name_uniq', 'unique (name)', ('Nombre del tipo de Vehículo ya existe')),
	]