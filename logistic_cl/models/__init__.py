# -*- encoding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from . import fleet_vehicle
from . import fleet_vehicle_type
from . import logistic_requirement
from . import waybill

