# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in module root
# directory
##############################################################################


from openerp import models, fields, api
from openerp import netsvc
from openerp.exceptions import Warning
from openerp.tools.translate import _

class requirement(models.Model):

    _inherit = 'logistic.requirement'   
    
    # Campos Nuevos Documentos Conductor     
    clase = fields.Char(string='Clase', required=False)    
    restrictions = fields.Char(string='Restricciones', required=False)  
    specialty = fields.Char(string='Especialidad', required=False)  
    inscription = fields.Char(string='Inscription', required=False)
    rut = fields.Char(related='partner_id.vat', string='Rut', required=False) 
    city = fields.Char(related='partner_id.city', string='Municipalidad', required=False)
    street = fields.Char(related='partner_id.street', string='Direccion', required=False)
    fecha_ultimo_control = fields.Date(string='Fecha Último Control',required=False)
    fecha_control = fields.Date(string='Fecha Control',required=False)

    # Campos Nuevos Documentos Conductor
    motor = fields.Char(string='Motor', required=False)
    licencia = fields.Char(string='Nro Licencia', required=False)
    year = fields.Integer(related='vehicle_id.year',string='Year', required=False)
    chassis = fields.Char(related='vehicle_id.vin_sn',string='Number Chassis', required=False)
    serie = fields.Char(string='Number Serie', required=False)    
    color = fields.Char(related='vehicle_id.color',string='Color', required=False)      
    run = fields.Char(string='Run', required=False)
    vin = fields.Char(string='Number Vin', required=False) 
    type_vehicle = fields.Selection(related='vehicle_id.type', string='Tipo Vehículo')  
    #type_vehicle = fields.Selection([('car', 'Car'), ('tractor', 'Tractor'), ('wagon', 'Wagon')], string='Type Vehicle') 
    

    