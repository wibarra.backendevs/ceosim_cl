# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2009  Àngel Àlvarez - NaN  (http://www.nan-tic.com)
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Stock de Productos por bodega',
    'version': '8.0',
    'category': 'Stock',
    'sequence': 14,
    'summary': 'Reporte de Stock de productos por bodega',
    'description': """
Stock en Bodega
============
:Stock de Inventario
----------
* Stock de Inventario por Bodega con la siguiente información:
- Nombre de la Bodega o WH
- ID Producto
- Nombre Producto
- Código EAN13
- Stock en bodega
- Por Entrar
- Cantidad Prevista """,
    'author':  'Wilmer Ibarra',
    'license': 'AGPL-3',
    'images': [
    ],
    'depends': [
        'stock',
    
    ],
    'data': [
        'views/stock.xml',
    ],
    'demo': [
        #'demo/demo_data.xml',
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
