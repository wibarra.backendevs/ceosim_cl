#-*- coding: utf-8 -*-
from openerp import models, fields, api

class StockQuant(models.Model):
    _inherit = 'stock.quant'

    #id_product = fields.Integer(related="product_id.id", store=True,)
    name_product = fields.Char(related="product_id.name", store=True,)
    ean13 = fields.Char(related="product_id.ean13", store=True,)
    qty_available = fields.Float(related="product_id.qty_available", store=True,)
    incoming_qty = fields.Float(related="product_id.incoming_qty", store=True,)
    virtual_available = fields.Float(related="product_id.virtual_available", store=True,)