odoo.define('webclient_template_cl.base_extend', function (require) {

	'use strict';

	var core = require('web.core');

	var ajax = require('web.ajax');

	var qweb = core.qweb;

	ajax.loadXML('/webclient_template_cl/static/src/xml/base_extend.xml', qweb);

});