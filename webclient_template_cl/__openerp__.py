# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2009  Àngel Àlvarez - NaN  (http://www.nan-tic.com)
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Branding Nueva Gestion',
    'version': '8.0',
    'category': 'Web',
    'sequence': 14,
    'summary': 'Branding Nueva Gestión',
    'description': """
Remover el branding de Odoo
============
remover el branding de odoo:
----------
* Se utiliza el módulo support_branding pero le falta detalles que quitar.

Ejemplo:
* Acerca de Odoo (en el perfil)

* Odoo en el Favicon

* Odoo al iniciar sesión

* Etc 
    """,
    'author':  'Wilmer Ibarra',
    'license': 'AGPL-3',
    'images': [
    ],
    'depends': [
        'web',
    
    ],
    'data': [
        'views/webclient_template_inherit.xml',        
    ],
    'qweb': [
        'static/src/xml/base_extend.xml',
    ],
    'demo': [
        #'demo/demo_data.xml',
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
